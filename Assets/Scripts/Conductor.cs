using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Conductor : MonoBehaviour
{
    private float songBPM;
    private float secPerBeats;
    private float _songPos;
    public float songPos { get { return _songPos; } }
    private int _songPosInBeats;
    public int songPosInBeats { get { return _songPosInBeats; } }
    private float dspSongStartTime;
    private AudioSource audioSource;
    private int _lastBeat;
    public int lastBeat { get { return _lastBeat; } }
    private bool hasStarted = false;
    public int intervalBetweenBeatEvents = 2;

    public delegate void BeatAction();
    public static event BeatAction OnBeatEvent;

    public static bool isReady { get; private set; } = false;

    public delegate void MusicStartingAction();
    public static event MusicStartingAction OnMusicStarted;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        audioSource = GetComponent<AudioSource>();

        // Get BPM of song
        var audioFilePath = AssetDatabase.GetAssetPath(audioSource.clip.GetInstanceID());
        var audioFile = TagLib.File.Create(audioFilePath);
        songBPM = audioFile.Tag.BeatsPerMinute;

        secPerBeats = 60f / songBPM;

        isReady = true;
        
        yield return new WaitForSeconds(3f);

        dspSongStartTime = (float)AudioSettings.dspTime;
        audioSource.Play();
        hasStarted = true;
        if (OnMusicStarted != null)
            OnMusicStarted();
    }

    // Update is called once per frame
    void Update()
    {
        if (hasStarted)
        {
            // Re-determine how many seconds since the song started
            _songPos = (float)(AudioSettings.dspTime - dspSongStartTime);

            // Re-determine how many beats since the song started
            _songPosInBeats = (int)(songPos / secPerBeats);

            if (_lastBeat != _songPosInBeats) // If new beat
            {
                _lastBeat = _songPosInBeats;
                if (_songPosInBeats % intervalBetweenBeatEvents == 0 && OnBeatEvent != null)
                {
                    OnBeatEvent();
                }
            }
        }
    }
}
