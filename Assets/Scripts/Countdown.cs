using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Countdown : MonoBehaviour
{
    private TMP_Text textMesh;
    private int currentCountdown = 3;

    private void Start()
    {
        textMesh = GetComponent<TextMeshProUGUI>();
        while (!Conductor.isReady)
            ;
        StartCoroutine(UpdateCountdown());
        
    }

    IEnumerator UpdateCountdown()
    {
        Debug.Log(textMesh);
        while (currentCountdown >= 0)
        {
            textMesh.text = currentCountdown.ToString();
            currentCountdown--;
            yield return new WaitForSeconds(1f);
        }
        Destroy(gameObject);
    }
}
