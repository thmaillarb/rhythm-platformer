using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed;
    private Rigidbody2D rb;
    private CapsuleCollider2D boxCollider;
    private Vector3 velocity = Vector3.zero;
    public float smoothTimeMovement = .05f;
    public bool isInMidair = false;
    public bool isJumping = false;
    public float jumpForce = 300;
    private float horizontalMovement;
    public Transform groundCheck;
    public Transform innerCheck;
    public LayerMask collisionLayer;
    private Animator animator;
    private SpriteRenderer spriteRenderer;
    private bool canMove = false;
    private GameObject deathAltitude;
    private GameObject spawnPoint;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        boxCollider = GetComponent<CapsuleCollider2D>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void OnEnable()
    {
        Conductor.OnMusicStarted += EnableMovement;
        SceneManager.sceneLoaded += FetchDeathAltitude;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= FetchDeathAltitude;
    }

    void Update()
    {
        if (canMove)
        {
            bool oldIsInMidair = isInMidair;
            // Is there a collision only with the floor?
            isInMidair = !Physics2D.OverlapBox(groundCheck.position, new Vector2(boxCollider.size.normalized.x, 0.5f), 0, collisionLayer) ||
                         Physics2D.OverlapBox(innerCheck.position, new Vector2(0.1f, 0.1f), 0, collisionLayer);

            if (oldIsInMidair != isInMidair)
                animator.SetTrigger("OnLand");

            horizontalMovement = Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime;

            if (Input.GetButtonDown("Jump") && !isInMidair)
            {
                isJumping = true;
                animator.SetTrigger("OnJump");
            }

            if (deathAltitude != null && spawnPoint != null && gameObject.transform.position.y <= deathAltitude.transform.position.y)
            {
                gameObject.transform.position = spawnPoint.transform.position;
            }
        }
    }

    void FixedUpdate()
    {
        if (canMove)
        {
            MovePlayer();
            animator.SetFloat("HSpeed", Mathf.Abs(rb.velocity.x));
            spriteRenderer.flipX = rb.velocity.x < -0.1f;
        }
    }

    void MovePlayer()
    {
        Vector3 targetVelocity = new Vector2(horizontalMovement, rb.velocity.y);
        rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref velocity, smoothTimeMovement);

        if (isJumping)
        {
            rb.AddForce(new Vector2(0f, jumpForce));
            isJumping = false;
        }
    }

    void OnDrawGizmos()
    {
        rb = GetComponent<Rigidbody2D>();
        boxCollider = GetComponent<CapsuleCollider2D>();
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(groundCheck.position, new Vector3(boxCollider.size.x * boxCollider.size.normalized.x, 0.5f, 0));
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(innerCheck.position, new Vector3(0.1f, 0.1f));
    }

    void EnableMovement()
    {
        canMove = true;
        Conductor.OnMusicStarted -= EnableMovement;
    }

    public void FetchDeathAltitude(Scene _, LoadSceneMode __)
    {
        deathAltitude = GameObject.FindGameObjectWithTag("deathAltitude");
        spawnPoint = GameObject.FindGameObjectWithTag("spawnPoint");
    }
}
