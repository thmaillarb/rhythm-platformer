using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ToggleTilemap : MonoBehaviour
{
    TilemapRenderer tilemapRenderer;
    TilemapCollider2D tilemapCollider;

    private void OnEnable()
    {
        tilemapRenderer = GetComponent<TilemapRenderer>();
        tilemapCollider = GetComponent<TilemapCollider2D>();
        Conductor.OnBeatEvent += Toggle;
    }

    private void OnDisable()
    {
        Conductor.OnBeatEvent -= Toggle;
    }

    void Toggle()
    {
        tilemapRenderer.enabled = !tilemapRenderer.enabled;
        tilemapCollider.enabled = !tilemapCollider.enabled;
    }
}
