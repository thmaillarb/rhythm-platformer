# Rythm platformer

Prototype d'un jeu de plate-forme 2D,
avec des parties du niveau apparaissant et disparaissant en rythme.

## Ressources utilisées

- [OPP2017 Cave and Mine Cart par Hapiel](https://opengameart.org/content/opp2017-cave-and-mine-cart)
- [TagLib-Sharp](https://github.com/mono/taglib-sharp)
- [Free Rhythm Game Music Pack 1 par tricksntraps](https://opengameart.org/content/free-rhythm-game-music-pack-1) (spécifiquement le morceau Ascend)
- [Free 2D Mega Pack par Brackey](https://assetstore.unity.com/packages/2d/free-2d-mega-pack-177430)
